# CS-DataMining-22-23

This project is using Python 3.10 :snake: Find more info [here](https://python.org/ "Official website")

## Markdown practice section

___

### Different text formatting styles

This is a regular text

*This is an italic text*

**This is a bold text**

***This text is both bold and italic***

Here I will put *some text* that can be considered a **longer** sentence 
just to see the in-line markdown formatting ***possibilities***.

___

### Lists

- Item 2
- Item 4
- Item 5
- Item 6
- Item 1
- Item 3

1. Item 1
2. Item 2
3. Item 3
4. Item 4
5. Item 5
6. Item 6

___

### Tables

|       | Column 1    | Column 2    |
|-------|-------------|-------------|
| Row 1 | datapoint11 | datapoint12 |
| Row 2 | datapoint21 | datapoint22 |


---

### Equations

Circle length: $l = 2\pi \times R$

Circle area: $\pi \times R^2$

Differential equation:

$$
  x_1 \frac{\partial y}{\partial x_1} + x_2 \frac{\partial y}{\partial x_2} = y
$$

Integral

$$
  \int_0^\frac{\pi}{2} sin(x) dx = 1 
$$

---

### Code blocks

To create and activate python venv run next command
> Make sure you have installed python3.10 first by running `which python3.10` or `python3.10 --version`'
```shell
  $ python3.10 -m venv .venv && . .venv/bin/activate || echo "Please check your python3.10 installation!"
```

